ALTER TABLE tb_users
DROP CONSTRAINT tb_users_pkey;
ALTER TABLE tb_users
    ADD CONSTRAINT tb_users_pkey PRIMARY KEY (id,username);
