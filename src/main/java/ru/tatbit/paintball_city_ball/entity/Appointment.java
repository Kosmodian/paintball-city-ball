package ru.tatbit.paintball_city_ball.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table(name = "tb_appointments")
public class Appointment {
    @Id
    @Column(name = "id")
    private Long id;
    @Column(name = "visit_date_time")
    private LocalDateTime visitDateTime;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User user;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Appointment appointment = (Appointment) o;
        return id != null && id.equals(appointment.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, user);
    }
}
