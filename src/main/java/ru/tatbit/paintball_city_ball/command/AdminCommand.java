package ru.tatbit.paintball_city_ball.command;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.tatbit.paintball_city_ball.entity.User;
import ru.tatbit.paintball_city_ball.repository.UserRepository;

@Component
@Data
@Slf4j
public class AdminCommand implements Command {
    private final UserRepository userRepository;

    @Override
    public String execute(Update update) {
        if (userRepository.checkRolesByUserId(update.getMessage().getChatId()).contains(User.Role.ADMIN)) {
            log.info("Successfully logged in admin panel");
            return "Вы находитесь в панели администратора";
        } else {
            log.error("Not authorities to log in admin panel");
            return "У вас нет прав администратора";
        }
    }
}
