package ru.tatbit.paintball_city_ball.command;

import lombok.Data;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.tatbit.paintball_city_ball.repository.UserRepository;

@Data
@Component
public class StartCommand implements Command {
    private final UserRepository userRepository;

    @Override
    public String execute(Update update) {
        if (userRepository.findById(update.getMessage().getChat().getId()).isEmpty()) {
            return "Здравствуйте!" + "\n" + "Если вы хотите записаться на игру, пожалуйста пройдите регистрацию.";
        } else {
            return "Здравствуйте, " + update.getMessage().getChat().getFirstName();
        }
    }
}
