package ru.tatbit.paintball_city_ball.command;

import lombok.Data;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.objects.Update;

@Data
@Service
public class TariffsCommand implements Command {
    @Override
    public String execute(Update update) {
        return "1 час игры (форма, маркер, 100 шаров) - 500р\n"
                + "Дополнительные шары 2р - 1 шар\n"
                + "гранаты и дымовые шашки 150р - 1 шт.\n";
    }
}
