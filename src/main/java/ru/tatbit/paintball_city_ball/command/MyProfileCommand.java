package ru.tatbit.paintball_city_ball.command;

import lombok.Data;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.tatbit.paintball_city_ball.entity.User;
import ru.tatbit.paintball_city_ball.repository.UserRepository;

import java.util.Locale;
import java.util.Optional;

@Data
@Component
public class MyProfileCommand implements Command {
    private final UserRepository userRepository;

    @Override
    public String execute(Update update) {
        Optional<User> userFromDb = userRepository.findById(update.getMessage().getChat().getId());
        StringBuilder sb = new StringBuilder();
        buildUser(sb, userFromDb);

        if (userFromDb.isEmpty()) {
            sb.append("Фамилия: ")
                    .append(update.getMessage().getChat().getLastName() == null ? "no_lastname" : update.getMessage().getChat().getLastName())
                    .append("\n")
                    .append("Имя: ")
                    .append(update.getMessage().getChat().getFirstName() == null ? "no_firstname" : update.getMessage().getChat().getFirstName())
                    .append("\n")
                    .append("Username: ")
                    .append(update.getMessage().getChat().getUserName() == null ? "no_username" : update.getMessage().getChat().getUserName());
        }
        return sb.toString();
    }

    public void buildUser(StringBuilder sb, Optional<User> userFromDb) {
        userFromDb.ifPresent(user -> sb.append("Фамилия: ")
                .append(user.getSurname())
                .append("\n")
                .append("Имя: ")
                .append(user.getName())
                .append("\n")
                .append("Username: ")
                .append(user.getUsername())
                .append("\n")
                .append("Пол: ")
                .append(user.getSex())
                .append("\n")
                .append("Телефон: ")
                .append(user.getPhone())
                .append("\n")
                .append("Дата рождения: ")
                .append(user.getDateOfBirth()));
    }

    public String getAccountInfoById(long id) {
        StringBuilder sb = new StringBuilder();

        Optional<User> userFromDb = userRepository.findById(id);
        buildUser(sb, userFromDb);

        if (userFromDb.isEmpty()) {
            sb.append("Пользователь не найден");
        }

        return sb.toString();

    }
}
