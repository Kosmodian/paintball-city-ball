package ru.tatbit.paintball_city_ball.command;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.tatbit.paintball_city_ball.entity.User;
import ru.tatbit.paintball_city_ball.repository.UserRepository;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

//@Component
//@Data
//@Slf4j
//public class DataCommand implements Command {
//    private final UserRepository userRepository;
//
//
//    @Override
//
//    public String execute(Update update) {
//        Message message = update.getMessage();
//        if (userRepository.findById(message.getChat().getId()).isEmpty()) {
//            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
//            String text = message.getText();
//            String[] data = text.split(" ");
//            boolean arrayLength = data.length == 6;
//            try {
//                if (data.length != 6 && data.length != 5) {
//                    throw new RuntimeException();
//                }
//                Date resultDateOfBirth = format.parse(arrayLength ? data[4] : data[3]);
//
//                User user = User.builder()
//                        .surname(message.getChat().getLastName())
//                        .name(message.getChat().getFirstName())
//                        .username(message.getChat().getUserName())
//                        .phone(arrayLength ? data[3] : data[2])
//                        .dateOfBirth(resultDateOfBirth)
//                        .sex(arrayLength ?
//                                (data[5].equalsIgnoreCase("мужской") ?
//                                        User.Sex.MALE : (data[5].equalsIgnoreCase("женский") ?
//                                        User.Sex.FEMALE : null))
//                                : (data[4].equalsIgnoreCase("мужской") ?
//                                User.Sex.MALE : (data[4].equalsIgnoreCase("женский") ?
//                                User.Sex.FEMALE : null)))
//                        .status(User.Status.UNCONFIRMED)
//                        .build();
//
//                userRepository.save(user);
//                log.info("{} successfully saved", user);
//                return "Регистрация окончена";
//            } catch (RuntimeException e) {
//                log.error(e.getMessage());
//                return "Неверные данные";
//            } catch (Exception e) {
//                log.error("User {} sent invalid data {}", message.getChat().getUserName(), Arrays.toString(data));
//                return "Неверные данные";
//            }
//        } else {
//            log.error("User can have single registration");
//            return "Пользователь уже зарегистрирован";
//        }
//    }
//}
//
//
