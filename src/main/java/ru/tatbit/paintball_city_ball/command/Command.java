package ru.tatbit.paintball_city_ball.command;

import org.telegram.telegrambots.meta.api.objects.Update;

public interface Command {
    String execute (Update update);
}
