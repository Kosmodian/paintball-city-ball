package ru.tatbit.paintball_city_ball.command;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.tatbit.paintball_city_ball.repository.UserRepository;

@Service
@Data
@Slf4j
public class AppointmentCommand implements Command {
    private final UserRepository userRepository;

    @Override
    public String execute(Update update) {
        if (userRepository.findById(update.getMessage().getChatId()).isPresent()) {
            return "Выберите дату приема";

        } else {
            return "Необходимо зарегистрироваться";
        }
    }
}
