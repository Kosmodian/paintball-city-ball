package ru.tatbit.paintball_city_ball.command;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.tatbit.paintball_city_ball.entity.User;
import ru.tatbit.paintball_city_ball.repository.UserRepository;

@Service
@Data
@Slf4j
public class RecordsCalendarCommand implements Command {
    private final UserRepository userRepository;

    @Override
    public String execute(Update update) {
        if (userRepository.checkRolesByUserId(update.getMessage().getChatId()).contains(User.Role.ADMIN)) {
            log.info("Successfully logged in admin panel");
            return "Выберите месяц";
        } else {
            log.error("Not authorities to log in admin panel");
            return "У вас нет прав администратора";
        }
    }

}
