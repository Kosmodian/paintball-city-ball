package ru.tatbit.paintball_city_ball.command;

import lombok.Data;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.objects.Update;

@Data
@Service
public class AboutTheSiteCommand implements Command {
    @Override
    public String execute(Update update) {
        return "Пейнтбольнй клуб расположен на территории спортивного центра «Динамо» в 14 км от центра Казани, " +
                "между поселками Мирный и Петровский, в экологически чистой природной зоне, " +
                "на благоустроенной охраняемой территории.\n" +
                "КАРТИНКА\n" +
                "В качестве дополнительных услуг, предоставляем беседку для отдыха и зону барбекю\n" +
                "КАРТИНКА\n";
    }
}
