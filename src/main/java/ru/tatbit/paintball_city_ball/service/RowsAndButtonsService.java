package ru.tatbit.paintball_city_ball.service;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@RequiredArgsConstructor
@Service
public class RowsAndButtonsService {
    protected static final String REGISTER_BUTTON = "REGISTER_BUTTON";
    protected static final String SEX_BUTTON = "SEX_BUTTON";
    protected static final String PHONE_BUTTON = "PHONE_BUTTON";
    protected static final String DATE_OF_BIRTH_BUTTON = "DATE_OF_BIRTH_BUTTON";
    protected static final String MALE_BUTTON = "MALE_BUTTON";
    protected static final String FEMALE_BUTTON = "FEMALE_BUTTON";
    protected static final String SIGN_UP_FOR_GAME_BUTTON = "SIGN_UP_FOR_GAME_BUTTON";
    protected static final String TARIFFS_BUTTON = "TARIFFS_BUTTON";
    protected static final String INFRASTRUCTURE_BUTTON = "INFRASTRUCTURE_BUTTON";
    protected static final String ABOUT_THE_SITE_BUTTON = "ABOUT_THE_SITE_BUTTON";
    protected static final String CONTACTS_BUTTON = "CONTACTS_BUTTON";
    protected static final String CALENDAR_OF_GAME_RECORDING_BUTTON = "CALENDAR_OF_GAME_RECORDING_BUTTON";
    protected static final String ADMIN_WEBSITE_BUTTON = "ADMIN_WEBSITE_BUTTON";
    protected static final String MY_PROFILE_BUTTON = "MY_PROFILE_BUTTON";
    protected static final String HISTORY_BUTTON = "HISTORY_BUTTON";
    protected static final String MAIN_MENU_BUTTON = "MAIN_MENU_BUTTON";
    protected static final String CUSTOMERS_BUTTON = "CUSTOMERS_BUTTON";
    protected static final String ADMIN_MAIN_MENU_BUTTON = "ADMIN_MAIN_MENU_BUTTON";
    protected static final String USER_MAIN_MENU_BUTTON = "USER_MAIN_MENU_BUTTON";


    public List<List<InlineKeyboardButton>> addRows(int rowsNumber) {
        List<List<InlineKeyboardButton>> rowsInLine = new ArrayList<>();
        for (int i = 0; i < rowsNumber; i++) {
            List<InlineKeyboardButton> row = new ArrayList<>();
            rowsInLine.add(row);
        }
        return rowsInLine;
    }

    public InlineKeyboardButton addButtonToRow(String text, String buttonName) {
        InlineKeyboardButton button = new InlineKeyboardButton();
        button.setText(text);
        button.setCallbackData(buttonName);

        return button;
    }

    public KeyboardButton addButtonToRow(String text, boolean isSimpleButton) {
        if (isSimpleButton) {
            KeyboardButton button = new KeyboardButton();
            button.setText(text);

            return button;
        }
        return null;
    }

    public SendMessage createSendMessage(long chatId, String text) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(chatId);
        sendMessage.setText(text);

        return sendMessage;
    }

    private void applyKeyBoardToMessage(SendMessage result, String buttonId, String buttonName) {
        InlineKeyboardMarkup markUpInLine = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowsInLine = addRows(1);
        rowsInLine.get(0).add(addButtonToRow(buttonName, buttonId));

        markUpInLine.setKeyboard(rowsInLine);

        result.setReplyMarkup(markUpInLine);
    }

    public void applyAdminMainMenuInlineKeyboardToMessage(SendMessage result) {
        applyKeyBoardToMessage(result, ADMIN_MAIN_MENU_BUTTON, "В главное меню");
    }
    public void applyUserMainMenuInlineKeyboardToMessage(SendMessage result) {
        applyKeyBoardToMessage(result, USER_MAIN_MENU_BUTTON, "В главное меню");
    }

    public InlineKeyboardMarkup applyAdminMainMenuInlineKeyboardToMessage() {
        InlineKeyboardMarkup markUpInLine = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowsInLine = addRows(1);
        rowsInLine.get(0).add(addButtonToRow("В главное меню", MAIN_MENU_BUTTON));

        markUpInLine.setKeyboard(rowsInLine);


        return markUpInLine;
    }

    public SendMessage applyMainMenuInlineKeyboardToMessage(long chatId, String response) {
        SendMessage sendMessage = createSendMessage(chatId, response);

        InlineKeyboardMarkup markupInLine = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowsInLine = addRows(1);

        rowsInLine.get(0).add(addButtonToRow("В главное меню", MAIN_MENU_BUTTON));

        markupInLine.setKeyboard(rowsInLine);
        sendMessage.setReplyMarkup(markupInLine);

        return sendMessage;
    }
}
