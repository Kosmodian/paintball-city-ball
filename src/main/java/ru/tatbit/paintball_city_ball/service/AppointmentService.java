package ru.tatbit.paintball_city_ball.service;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import ru.tatbit.paintball_city_ball.entity.Appointment;
import ru.tatbit.paintball_city_ball.repository.AppointmentRepository;
import ru.tatbit.paintball_city_ball.repository.UserRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Service
@Data
@Slf4j
public class AppointmentService {
    private final AppointmentRepository appointmentRepository;
    private final UserRepository userRepository;
    private final RowsAndButtonsService rowsAndButtons;

    public void saveAppointment(Appointment appointment) {
        appointmentRepository.save(appointment);
    }

    public LocalDateTime setVisitDateTime(String date, String time) {
        LocalDate resultDate = processDate(date);
        LocalTime resultTime = processTime(time);

        return LocalDateTime.of(resultDate, resultTime);


    }

    private LocalTime processTime(String time) {
        log.info("process time parameter: {}", time);
        String[] timeArray = null;
        Integer hours;
        Integer minutes;
        if (time.matches("\\d{2}\\.\\d{2}")) {
            timeArray = time.split("\\.");
        }

        if (time.matches("\\d{2}:\\d{2}")) {
            timeArray = time.split(":");
        }

        if (time.matches("\\d{2}-\\d{2}")) {
            timeArray = time.split("-");
        }

        if (time.matches("\\d{2} \\d{2}")) {
            timeArray = time.split(" ");
        }
        hours = Integer.parseInt(timeArray[0]);
        minutes = Integer.parseInt(timeArray[1]);
        log.info("process time result: {}", LocalTime.of(hours, minutes));
        return LocalTime.of(hours, minutes);

    }

    public LocalDate processDate(String date) {
        String[] dateArray;
        if (date.contains("_")) {
            dateArray = date.split("_");

            Month month = null;
            switch (dateArray[3]) {
                case "ЯНВАРЬ" -> month = Month.JANUARY;
                case "ФЕВРАЛЬ" -> month = Month.FEBRUARY;
                case "МАРТ" -> month = Month.MARCH;
                case "АПРЕЛЬ" -> month = Month.APRIL;
                case "МАЙ" -> month = Month.MAY;
                case "ИЮНЬ" -> month = Month.JUNE;
                case "ИЮЛЬ" -> month = Month.JULY;
                case "АВГУСТ" -> month = Month.AUGUST;
                case "СЕНТЯБРЬ" -> month = Month.SEPTEMBER;
                case "ОКТЯБРЬ" -> month = Month.OCTOBER;
                case "НОЯБРЬ" -> month = Month.NOVEMBER;
                case "ДЕКАБРЬ" -> month = Month.DECEMBER;
            }
            return LocalDate.of(Integer.parseInt(dateArray[4]), month, Integer.parseInt(dateArray[0]));
        } else {
            dateArray = date.split(" ");

            Month month = null;
            switch (dateArray[1]) {
                case "января" -> month = Month.JANUARY;
                case "февраля" -> month = Month.FEBRUARY;
                case "марта" -> month = Month.MARCH;
                case "апреля" -> month = Month.APRIL;
                case "мая" -> month = Month.MAY;
                case "июня" -> month = Month.JUNE;
                case "июля" -> month = Month.JULY;
                case "августа" -> month = Month.AUGUST;
                case "сентября" -> month = Month.SEPTEMBER;
                case "октября" -> month = Month.OCTOBER;
                case "ноября" -> month = Month.NOVEMBER;
                case "декабря" -> month = Month.DECEMBER;

            }
            return LocalDate.of(Integer.parseInt(dateArray[2]), month, Integer.parseInt(dateArray[0]));
        }
    }

    public boolean processAppointmentData(String dateTime, long chatId) {
        String[] data = dateTime.split(",");

        LocalDateTime resultDateTime = setVisitDateTime(data[0], data[1]);

        LocalDateTime prev = resultDateTime.minusMinutes(15);
        LocalDateTime next = resultDateTime.plusMinutes(15);

        List<Appointment> nearestAppointments = appointmentRepository.findByVisitDateTimeBetween(prev, next);

        if (nearestAppointments.isEmpty()) {

            Appointment appointment = new Appointment();

            appointment.setUser(userRepository.findById(chatId).orElseThrow(() -> new RuntimeException("User wasn't found")));
            appointment.setVisitDateTime(resultDateTime);

            saveAppointment(appointment);

            return true;
        } else {
            return false;
        }
    }

    public SendMessage showUserAppointments(Update update) {

        List<Appointment> appointments = appointmentRepository.findByUser_Id(update.getMessage().getChatId());

        if (!appointments.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("eeee, dd MMMM yyyy, HH:mm");

            appointments.forEach(appointment -> {
                String text = appointment.getVisitDateTime().format(formatter);
                sb.append(text).append("\n");
            });
            return rowsAndButtons.createSendMessage(update.getMessage().getChatId(), sb.toString());
        } else {
            return rowsAndButtons.createSendMessage(update.getMessage().getChatId(), "Вы не записывались");
        }

    }

    public List<SendMessage> showAppointmentsForDay(Update update, LocalDateTime currentDay, LocalDateTime nextDay) {
        List<Appointment> appointments = appointmentRepository.findByVisitDateTimeBetween(currentDay, nextDay);

        if (!appointments.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("eeee, dd MMMM yyyy, HH:mm");

            List<SendMessage> result = new ArrayList<>();
            for (int i = 0; i < appointments.size(); i++) {

                String text = appointments.get(i).getVisitDateTime().format(formatter);
                String username = appointments.get(i).getUser().getUsername();
                sb.append("@").append(username).append(", ").append(text).append("_a").append("\n");

                InlineKeyboardMarkup markUpInLine = new InlineKeyboardMarkup();
                List<List<InlineKeyboardButton>> rowsInLine = rowsAndButtons.addRows(1);
                rowsInLine.get(0).add(rowsAndButtons.addButtonToRow("Информация", "user@" + appointments.get(i).getUser().getId()));
                rowsInLine.get(0).add(rowsAndButtons.addButtonToRow("Изменить расписание", sb.toString()));

                markUpInLine.setKeyboard(rowsInLine);

                SendMessage sendMessage = new SendMessage();
                sendMessage.setText(sb.toString());
                sendMessage.setChatId(update.getCallbackQuery().getMessage().getChatId());
                sendMessage.setReplyMarkup(markUpInLine);

                result.add(sendMessage);

                sb.setLength(0);
            }

            return result;

        } else {
            List<SendMessage> messages = new ArrayList<>();
            messages.add(rowsAndButtons.createSendMessage(update.getCallbackQuery().getMessage().getChatId(), "Записей нет"));
            return messages;
        }
    }

    public Appointment findByVisitDateTime(LocalDateTime oldAppointmentDateTime) {
        return appointmentRepository.findByVisitDateTime(oldAppointmentDateTime);
    }

    public void deleteOldAppointment(String cutOldAppointmentDateTime) {
        String[] data = cutOldAppointmentDateTime.split(", ");

        LocalDateTime dateTime = setVisitDateTime(data[2], data[3]);

        Appointment appointmentToDelete = findByVisitDateTime(dateTime);
        appointmentRepository.delete(appointmentToDelete);
    }


}
