package ru.tatbit.paintball_city_ball.service;

import lombok.Data;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import ru.tatbit.paintball_city_ball.command.MyProfileCommand;

import java.util.List;

import static ru.tatbit.paintball_city_ball.service.RowsAndButtonsService.HISTORY_BUTTON;
import static ru.tatbit.paintball_city_ball.service.RowsAndButtonsService.MAIN_MENU_BUTTON;

@Service
@Data
public class InformationService {
    private final MyProfileCommand profileCommand;
    private final RowsAndButtonsService rowsAndButtons;
    private final RecordsCalendarService recordsService;

    public SendMessage sendInformationMessage(long id) {
        String info = profileCommand.getAccountInfoById(id);

        return SendMessage.builder()
                .text(info)
                .chatId(id)
                .replyMarkup(InlineKeyboardMarkup.builder()
                        .keyboardRow(List.of(InlineKeyboardButton.builder()
                                        .callbackData(HISTORY_BUTTON)
                                        .text("История записей")
                                        .build(),
                                InlineKeyboardButton.builder()
                                        .callbackData(MAIN_MENU_BUTTON)
                                        .text("В главное меню")
                                        .build()))
                        .build())
                .build();
    }
}
