package ru.tatbit.paintball_city_ball.service;

import lombok.Data;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.time.LocalDate;
import java.time.Month;
import java.time.format.TextStyle;
import java.util.*;

import static ru.tatbit.paintball_city_ball.service.RowsAndButtonsService.*;

@Service
@Data
public class RecordsCalendarService {
    private final RowsAndButtonsService rowsAndButtons;
    private Month monthForRecord = null;
    private Integer yearForRecord = null;

    public SendMessage sendRecordsCalendarMessage(Update update, boolean isAdminChange) {
        return sendDayChoiceMessage(update, isAdminChange);
    }

    public SendMessage sendDayChoiceMessage(Update update, boolean isAdminChange) {
        GregorianCalendar calendar = new GregorianCalendar();
        if (monthForRecord == null && yearForRecord == null) {
            monthForRecord = LocalDate.now().getMonth();
            yearForRecord = LocalDate.now().getYear();
        }

        if (update.hasCallbackQuery()) {
            String command = update.getCallbackQuery().getData();
            if (command.equals("CALENDAR.NEXT_MONTH")) {
                monthForRecord = monthForRecord.plus(1L);
                calendar.set(Calendar.MONTH, monthForRecord.getValue());
            }
            if (command.equals("CALENDAR.PREV_MONTH")) {
                monthForRecord = monthForRecord.minus(1L);
                calendar.set(Calendar.MONTH, monthForRecord.getValue());
            }
        }
        InlineKeyboardMarkup markUpInLine = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowsInLine = new ArrayList<>();

        List<InlineKeyboardButton> header = new ArrayList<>();
        InlineKeyboardButton left = new InlineKeyboardButton();
        left.setCallbackData("CALENDAR.PREV_MONTH");
        left.setText("<");
        header.add(left);

        InlineKeyboardButton month = new InlineKeyboardButton();
        month.setCallbackData("BUTTON_" + monthForRecord.getDisplayName(TextStyle.FULL_STANDALONE, new Locale("ENGLISH")));
        month.setText(monthForRecord.getDisplayName(TextStyle.FULL_STANDALONE, Locale.getDefault()));
        header.add(month);

        InlineKeyboardButton right = new InlineKeyboardButton();
        right.setCallbackData("CALENDAR.NEXT_MONTH");
        right.setText(">");
        header.add(right);

        rowsInLine.add(header);

        rowsInLine.add(prepareNameDaysOfWeek());

        prepareWeeksOfMonth(rowsInLine, isAdminChange);

        markUpInLine.setKeyboard(rowsInLine);
        SendMessage sendMessage = new SendMessage(String.valueOf(update.getMessage() == null
                ? update.getCallbackQuery().getFrom().getId()
                : update.getMessage().getChatId()), "Выберите дату");
        sendMessage.setReplyMarkup(markUpInLine);


        return sendMessage;
    }

    private List<InlineKeyboardButton> prepareNameDaysOfWeek() {
        List<InlineKeyboardButton> daysOfWeek = new ArrayList<>();
        List<String> nameDaysOfWeek = List.of(
                "ПН", "ВТ", "СР", "ЧТ", "ПТ", "СБ", "ВС"
        );

        for (String s : nameDaysOfWeek) {
            InlineKeyboardButton day = new InlineKeyboardButton();
            day.setText(s);
            day.setCallbackData(s);
            daysOfWeek.add(day);
        }

        return daysOfWeek;
    }

    private void prepareWeeksOfMonth(List<List<InlineKeyboardButton>> calendar, boolean isAdminChange) {
        List<InlineKeyboardButton> weekOfMonth = new ArrayList<>();
        String text;
        String callbackData;
        int countDayOfWeek = 1;
        int currentDay = 1;
        while (countDayOfWeek <= monthForRecord.maxLength()) {
            InlineKeyboardButton day = new InlineKeyboardButton();
            if (LocalDate.now().getMonth().compareTo(monthForRecord) < 0) {
                if (LocalDate.of(LocalDate.now().getYear(), monthForRecord, 1).getDayOfWeek().getValue() > currentDay) {
                    text = " ";
                    callbackData = " ";
                } else {
                    text = String.valueOf(LocalDate.of(LocalDate.now().getYear(), monthForRecord, countDayOfWeek).getDayOfMonth());
                    callbackData = LocalDate.of(LocalDate.now().getYear(), monthForRecord, countDayOfWeek).getDayOfMonth() + "_DAY_MONTH_"
                            + monthForRecord + "_" + yearForRecord + (isAdminChange ? "_a" : "");
                    countDayOfWeek++;
                }
            } else if (LocalDate.now().getMonth().compareTo(monthForRecord) > 0) {
                text = " ";
                callbackData = " ";
                countDayOfWeek++;
            } else {
                if (LocalDate.of(LocalDate.now().getYear(), LocalDate.now().getMonth(), 1).getDayOfWeek().getValue() > currentDay) {
                    text = " ";
                    callbackData = " ";
                } else {
                    text = String.valueOf(LocalDate.of(LocalDate.now().getYear(), monthForRecord, countDayOfWeek).getDayOfMonth());
                    callbackData = LocalDate.of(LocalDate.now().getYear(), monthForRecord, countDayOfWeek).getDayOfMonth() + "_DAY_MONTH_" +
                            monthForRecord + "_" + yearForRecord + (isAdminChange ? "_a" : "");
                    countDayOfWeek++;
                }
            }
            day.setText(text);
            day.setCallbackData(callbackData);

            weekOfMonth.add(day);
            if (currentDay % 7 == 0 || countDayOfWeek - 1 == monthForRecord.maxLength()) {
                if (countDayOfWeek - 1 == monthForRecord.maxLength()) {
                    int i = weekOfMonth.size();
                    while (i < 7) {
                        InlineKeyboardButton emptyDay = new InlineKeyboardButton();
                        emptyDay.setText(" ");
                        emptyDay.setCallbackData(" ");
                        weekOfMonth.add(emptyDay);
                        i++;
                    }
                }
                calendar.add(weekOfMonth);
                weekOfMonth = new ArrayList<>();
            }

            currentDay++;
        }

    }

    public SendMessage sendUserOpportunitiesMessage(long chatId, String response) {
        SendMessage sendMessage = rowsAndButtons.createSendMessage(chatId, response);

        InlineKeyboardMarkup markupInLine = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowsInLine = rowsAndButtons.addRows(6);

        rowsInLine.get(0).add(rowsAndButtons.addButtonToRow("Записаться на игру", SIGN_UP_FOR_GAME_BUTTON));
        rowsInLine.get(1).add(rowsAndButtons.addButtonToRow("Тарифы", TARIFFS_BUTTON));
        rowsInLine.get(2).add(rowsAndButtons.addButtonToRow("Инфраструктура", INFRASTRUCTURE_BUTTON));
        rowsInLine.get(3).add(rowsAndButtons.addButtonToRow("О площадке ", ABOUT_THE_SITE_BUTTON));
        rowsInLine.get(4).add(rowsAndButtons.addButtonToRow("Контакты", CONTACTS_BUTTON));
        rowsInLine.get(5).add(rowsAndButtons.addButtonToRow("Мой профиль", MY_PROFILE_BUTTON));

        markupInLine.setKeyboard(rowsInLine);
        sendMessage.setReplyMarkup(markupInLine);

        return sendMessage;
    }

    public SendMessage sendAdminFunctionsList(long chatId, String response) {
        SendMessage sendMessage = rowsAndButtons.createSendMessage(chatId, response);

        InlineKeyboardMarkup markupInLine = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowsInLine = rowsAndButtons.addRows(2);

        rowsInLine.get(0).add(rowsAndButtons.addButtonToRow("Календарь записей игр", CALENDAR_OF_GAME_RECORDING_BUTTON));
        rowsInLine.get(1).add(rowsAndButtons.addButtonToRow("Веб-страница", ADMIN_WEBSITE_BUTTON));

        markupInLine.setKeyboard(rowsInLine);
        sendMessage.setReplyMarkup(markupInLine);

        return sendMessage;
    }

    //TODO:
    public InlineKeyboardMarkup adminKeyBoard() {
        InlineKeyboardMarkup markUpInLine = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowsInLine = rowsAndButtons.addRows(2);

        rowsInLine.get(0).add(rowsAndButtons.addButtonToRow("Календарь записей игр", CALENDAR_OF_GAME_RECORDING_BUTTON));
        rowsInLine.get(1).add(rowsAndButtons.addButtonToRow("Веб-страница", ADMIN_WEBSITE_BUTTON));

        markUpInLine.setKeyboard(rowsInLine);

        return markUpInLine;
    }

    //TODO:
    public SendMessage userKeyBoard(SendMessage result) {
        InlineKeyboardMarkup markupInLine = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowsInLine = rowsAndButtons.addRows(6);

        rowsInLine.get(0).add(rowsAndButtons.addButtonToRow("Записаться на игру", SIGN_UP_FOR_GAME_BUTTON));
        rowsInLine.get(1).add(rowsAndButtons.addButtonToRow("Тарифы", TARIFFS_BUTTON));
        rowsInLine.get(2).add(rowsAndButtons.addButtonToRow("Инфраструктура", INFRASTRUCTURE_BUTTON));
        rowsInLine.get(3).add(rowsAndButtons.addButtonToRow("О площадке ", ABOUT_THE_SITE_BUTTON));
        rowsInLine.get(4).add(rowsAndButtons.addButtonToRow("Контакты", CONTACTS_BUTTON));
        rowsInLine.get(5).add(rowsAndButtons.addButtonToRow("Мой профиль", MY_PROFILE_BUTTON));

        markupInLine.setKeyboard(rowsInLine);
        result.setReplyMarkup(markupInLine);
        result.setText("Выберите услугу");

        return result;
    }

//    public SendMessage sendMyProfileList(long chatId, String response) {
//        SendMessage sendMessage = rowsAndButtons.createSendMessage(chatId, response);
//
//        InlineKeyboardMarkup markupInLine = new InlineKeyboardMarkup();
//        List<List<InlineKeyboardButton>> rowsInLine = rowsAndButtons.addRows(2);
//
//        rowsInLine.get(0).add(rowsAndButtons.addButtonToRow("История записей", HISTORY_BUTTON));
//        rowsInLine.get(1).add(rowsAndButtons.addButtonToRow("В главное меню", MAIN_MENU_BUTTON));
//
//        markupInLine.setKeyboard(rowsInLine);
//        sendMessage.setReplyMarkup(markupInLine);
//
//        return sendMessage;
//    }

    public SendMessage sendCustomersAndScheduleMessage(long chatId, String response) {
        SendMessage sendMessage = rowsAndButtons.createSendMessage(chatId, response);
        InlineKeyboardMarkup markUpInLine = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowsInLine = rowsAndButtons.addRows(3);
        rowsInLine.get(0).add(rowsAndButtons.addButtonToRow("Клиенты", CUSTOMERS_BUTTON));
        rowsInLine.get(0).add(rowsAndButtons.addButtonToRow("В главное меню", MAIN_MENU_BUTTON));

        markUpInLine.setKeyboard(rowsInLine);
        sendMessage.setReplyMarkup(markUpInLine);

        return sendMessage;
    }

}
