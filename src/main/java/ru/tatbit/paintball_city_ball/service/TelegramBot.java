package ru.tatbit.paintball_city_ball.service;

import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.commands.SetMyCommands;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.commands.BotCommand;
import org.telegram.telegrambots.meta.api.objects.commands.scope.BotCommandScopeDefault;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.tatbit.paintball_city_ball.command.Command;
import ru.tatbit.paintball_city_ball.command.MyProfileCommand;
import ru.tatbit.paintball_city_ball.command.StartCommand;
import ru.tatbit.paintball_city_ball.config.BotConfig;
import ru.tatbit.paintball_city_ball.repository.UserRepository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@Slf4j
public class TelegramBot extends TelegramLongPollingBot {
    private final BotConfig botConfig;
    private Map<String, Command> command;
    private final CommandService commandService;


    public TelegramBot(BotConfig botConfig, UserRepository userRepository, CommandService commandService) {
        this.botConfig = botConfig;
        this.commandService = commandService;
        this.command = new HashMap<>();
        this.command.put("/start", new StartCommand(userRepository));
        this.command.put("/my_profile", new MyProfileCommand(userRepository));

        List<BotCommand> listOfCommand = List.of(
                new BotCommand("/start", "Get a welcome message"),
                new BotCommand("/my_profile", "Get you profile information"),
                new BotCommand("/admin", "Show the admin panel"),
                new BotCommand("/calendar", "Open the calendar for recording"),
                new BotCommand("/history", "Show game recording"),
                new BotCommand("/tariffs","Show prices for the game "),
                new BotCommand("/about_the_site", "Show the club's infrastructure")
        );
        setCommandMenu(listOfCommand);
    }

    @Override
    public void onUpdateReceived(Update update) {
        Optional<SendMessage> message = commandService.validateCommand(update);
        message.ifPresent(this::sendMessage);
    }

    @PostConstruct
    public void init() {
        commandService.registerBot(this);
    }


    public void sendMessage(BotApiMethod<?> sendMessage) {
        try {
            execute(sendMessage);
            log.info("{} successfully sent", sendMessage);
        } catch (TelegramApiException e) {
            log.error("error during sending {}", sendMessage);
            throw new RuntimeException();
        }
    }

    private void setCommandMenu(List<BotCommand> listOfCommand) {
        try {
            this.execute(new SetMyCommands(listOfCommand, new BotCommandScopeDefault(), null));
        } catch (TelegramApiException e) {
            log.error("Error setting bot command list: " + e);
        }
    }


    @Override
    public String getBotUsername() {
        return botConfig.getBotName();
    }

    @Override
    public String getBotToken() {
        return botConfig.getBotToken();
    }
}
