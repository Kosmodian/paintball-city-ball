package ru.tatbit.paintball_city_ball.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import ru.tatbit.paintball_city_ball.command.*;
import ru.tatbit.paintball_city_ball.entity.User;
import ru.tatbit.paintball_city_ball.repository.UserRepository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;

import static ru.tatbit.paintball_city_ball.service.RowsAndButtonsService.*;

@Service
@Slf4j
public class CommandService {
    private final UserRepository userRepository;
    private final Map<String, Command> command;
    private final RegisterService registerService;
    private static final Map<String, String> bio = new HashMap<>();
    private static final Map<String, String> appointment = new HashMap<>();
    private TelegramBot telegramBot;
    private final RowsAndButtonsService rowsAndButtons;
    private final RecordsCalendarService recordsService;
    private final AppointmentService appointmentService;
    private final InformationService informationService;

    public CommandService(UserRepository userRepository, RegisterService registerService, RowsAndButtonsService rowsAndButtons,
                          RecordsCalendarService calendarService, AppointmentService appointmentService, InformationService informationService) {
        this.userRepository = userRepository;
        this.registerService = registerService;
        this.rowsAndButtons = rowsAndButtons;
        this.recordsService = calendarService;
        this.appointmentService = appointmentService;
        this.informationService = informationService;

        this.command = new HashMap<>();
        this.command.put("/start", new StartCommand(userRepository));
        this.command.put("/my_profile", new MyProfileCommand(userRepository));
        this.command.put("/admin", new AdminCommand(userRepository));
        this.command.put("/calendar", new RecordsCalendarCommand(userRepository));
        this.command.put("/history", new HistoryCommand(userRepository));
        this.command.put("/tariffs", new TariffsCommand());
        this.command.put("/about_the_site", new AboutTheSiteCommand());
    }


    public Optional<SendMessage> validateCommand(Update update) {
        SendMessage result = new SendMessage();
        String response;
        if (update.hasMessage() && update.getMessage().hasText()) {
            String messageText = update.getMessage().getText();
            long chatId = update.getMessage().getChatId();

            switch (messageText) {
                case "/start" -> {
                    log.info("In start command: ");
                    Command startCommand = this.command.get("/start");
                    response = startCommand.execute(update);
                    bio.put("username", update.getMessage().getChat().getUserName());
                    bio.put("name", update.getMessage().getChat().getFirstName());
                    bio.put("surname", update.getMessage().getChat().getLastName() == null ? "no_surname" : update.getMessage().getChat().getLastName());
                    bio.put("chatId", String.valueOf(update.getMessage().getChatId()));

                    if (response.charAt(12) == '!') {
                        return Optional.ofNullable(registerService.sendInviteForRegisterMessage(chatId, response));
                    } else {
                        result.setText(response);
                        result.setChatId(chatId);
                        return Optional.ofNullable(recordsService.sendUserOpportunitiesMessage(chatId, response));
                    }
                }
                case "/my_profile" -> {
                    log.info("In my profile: ");
                    Command myProfileCommand = this.command.get("/my_profile");
                    response = myProfileCommand.execute(update);
                    result.setChatId(chatId);
                    result.setText(response);
                    result.setReplyMarkup(InlineKeyboardMarkup.builder()
                            .keyboardRow(List.of(InlineKeyboardButton.builder()
                                            .callbackData(HISTORY_BUTTON)
                                            .text("История записей")
                                            .build(),
                                    InlineKeyboardButton.builder()
                                            .callbackData(MAIN_MENU_BUTTON)
                                            .text("В главное меню")
                                            .build()))
                            .build());
                    return Optional.of(result);
                }
                case "/admin" -> {
                    log.info("In admin panel: ");
                    Command adminCommand = this.command.get("/admin");
                    response = adminCommand.execute(update);

                    if (!response.equals("У вас нет прав администратора")) {
                        return Optional.ofNullable(recordsService.sendAdminFunctionsList(chatId, response));
                    }
                    result.setText(response);
                    result.setChatId(chatId);

                }
                case "/calendar" -> {
                    log.info("In my calendar:");
                    Command calendar = this.command.get("/calendar");
                    response = calendar.execute(update);
                    if (response.startsWith("Выберите")) {
                        return Optional.ofNullable(recordsService.sendRecordsCalendarMessage(update, false));
                    }
                    result.setText(response);
                    result.setChatId(chatId);
                }
                case "/appointment" -> {
                    log.info("{} press appointment button", update.getMessage().getFrom());
                    Command appointment = this.command.get("/appointment");
                    response = appointment.execute(update);
                    if (response.startsWith("Выберите") || response.startsWith("Choose")) {
                        return Optional.ofNullable(recordsService.sendRecordsCalendarMessage(update, false));
                    }
                    result.setText(response);
                    result.setChatId(chatId);
                }
                case "/history" -> {
                    log.info("{} press history button", update.getMessage().getFrom());
                    Command history = this.command.get("/history");
                    response = history.execute(update);
                    if (response.startsWith("Ваши")) {
                        telegramBot.sendMessage(SendMessage.builder()
                                .chatId(chatId)
                                .text("Ваши записи:")
                                .build());
                        return Optional.ofNullable(appointmentService.showUserAppointments(update));
                    }
                    result.setText(response);
                    result.setChatId(chatId);
                }
                case "/tariffs" -> {
                    log.info("{} press tariffs button", update.getMessage().getFrom());
                    Command tariffs = this.command.get("/tariffs");
                    response = tariffs.execute(update);
                    result.setChatId(chatId);
                    result.setText(response);
                    result.setReplyMarkup(InlineKeyboardMarkup.builder()
                            .keyboardRow(List.of(InlineKeyboardButton.builder()
                                            .callbackData(SIGN_UP_FOR_GAME_BUTTON)
                                            .text("Записаться на игру")
                                            .build(),
                                    InlineKeyboardButton.builder()
                                            .callbackData(MAIN_MENU_BUTTON)
                                            .text("В главное меню")
                                            .build()))
                            .build());
                }
                case "/about_the_site" -> {
                    log.info("{} press about the site button", update.getMessage().getFrom());
                    Command aboutTheSite = this.command.get("/about_the_site");
                    response = aboutTheSite.execute(update);
                    result.setChatId(chatId);
                    result.setText(response);
                    result.setReplyMarkup(InlineKeyboardMarkup.builder()
                            .keyboardRow(List.of(InlineKeyboardButton.builder()
                                            .callbackData(SIGN_UP_FOR_GAME_BUTTON)
                                            .text("Записаться на игру")
                                            .build(),
                                    InlineKeyboardButton.builder()
                                            .callbackData(MAIN_MENU_BUTTON)
                                            .text("В главное меню")
                                            .build()))
                            .build());
                }

                default -> {
                    result.setChatId(chatId);

                    if (messageText.replaceAll(" ", "")
                            .matches("^\\+?\\d{11}$")) {
                        bio.put("phone", messageText);
                        result.setText("Номер телефона сохранен");
                        checkUserFields(result);
                    } else if (messageText.matches("^\\d{2}-\\d{2}-\\d{4}$") ||
                            messageText.matches("^\\d{2}\\.\\d{2}\\.\\d{4}$") ||
                            messageText.matches("^\\d{2}\\.\\d{2}\\.\\d{2}$") ||
                            messageText.matches("^\\d{2}-\\d{2}-\\d{2}$")) {
                        bio.put("dateOfBirth", messageText);
                        result.setText("Дата рождения сохранена");
                        checkUserFields(result);
                    } else if ("Мужской".equalsIgnoreCase(messageText) || ("Женский".equalsIgnoreCase(messageText))) {
                        bio.put("sex", messageText);
                        result.setText("Пол сохранен");
                        checkUserFields(result);
                    } else if (messageText.matches("/admin \\d{2}\\.\\d{2}") ||
                            messageText.matches("/admin \\d{2}:\\d{2}") ||
                            messageText.matches("/admin \\d{2}-\\d{2}") ||
                            messageText.matches("/admin \\d{2} \\d{2}")) {
                        String cutMessage = messageText.substring(7);
                        String date = appointment.get("adminChangeDateTime");
                        String cutDate = date.replaceAll("_a", "");
                        String putDateTime = cutDate + "," + cutMessage;
                        appointmentService.processAppointmentData(putDateTime, chatId);

                        String oldAppointmentDateTime = appointment.get("adminRemoveDateTime");
                        String cutOldAppointmentDateTime = oldAppointmentDateTime.replaceAll("_a\n", "");
                        appointmentService.deleteOldAppointment(cutOldAppointmentDateTime);
                        appointment.remove("adminChangeDateTime");
                        rowsAndButtons.applyAdminMainMenuInlineKeyboardToMessage(result);
                        result.setText("Запись перенесена");

                    } else if (messageText.matches("\\d{2}\\.\\d{2}") ||
                            messageText.matches("\\d{2}:\\d{2}") ||
                            messageText.matches("\\d{2}-\\d{2}") ||
                            messageText.matches("\\d{2} \\d{2}")) {
                        appointment.put("dateTime", appointment.get("dateTime") + "," + messageText);
                        boolean isAppointmentAvailable = appointmentService.processAppointmentData(appointment.get("dateTime"), chatId);
                        appointment.remove("dateTime");
                        rowsAndButtons.applyUserMainMenuInlineKeyboardToMessage(result);
                        if (isAppointmentAvailable) {
                            result.setText("Вы успешно записались");
                        } else {
                            result.setText("Выберите другое время или дату");
                        }
                    } else {
                        result.setText("Неизвестная команда");
                    }
                }
            }
        } else if (update.hasCallbackQuery()) {
            EditMessageText message = registerService.checkCallbackQueryFromUser(update);
            long chatIdFromQuery = update.getCallbackQuery().getMessage().getChatId();
            result.setChatId(chatIdFromQuery);

            if (message.getText().matches(".+DAY_MONTH_.+_.+_a")) {

                appointment.put("adminChangeDateTime", message.getText());
                return Optional.ofNullable(sendMessage(chatIdFromQuery, "Введите новое время посещения в формате /admin ЧЧ.ММ, /admin ЧЧ:ММ, /admin ЧЧ-ММ или /admin ЧЧ ММ"));
            } else if (message.getText().matches(".+DAY_MONTH_.+_.+") && userRepository.checkRolesByUserId(chatIdFromQuery).contains(User.Role.ADMIN)) {

                appointment.put("adminDateTime", message.getText());
                return Optional.ofNullable(recordsService.sendCustomersAndScheduleMessage(chatIdFromQuery,
                        "Выберите действие"));
            } else if (message.getText().matches(".+DAY_MONTH_.+_.+") && userRepository.checkRolesByUserId(chatIdFromQuery).contains(User.Role.USER)) {

                appointment.put("dateTime", message.getText());
                return Optional.ofNullable(sendMessage(chatIdFromQuery, "Введите время посещения в формате ЧЧ.ММ, ЧЧ:ММ, ЧЧ-ММ или ЧЧ ММ"));
            } else if (message.getText().startsWith("@") && message.getText().contains("_a") && userRepository.checkRolesByUserId(chatIdFromQuery).contains(User.Role.ADMIN)) {

                appointment.put("adminRemoveDateTime", message.getText());
                return Optional.ofNullable(recordsService.sendRecordsCalendarMessage(update, true));
            } else if (message.getText().matches("user@.+")) {
                long userId = Long.parseLong(message.getText().substring(message.getText().indexOf("@") + 1));

                return Optional.ofNullable(informationService.sendInformationMessage(userId));
            }

            switch (message.getText()) {
                case "REGISTER_BUTTON" -> {
                    return Optional.ofNullable(registerService.sendRegisterMessage(update.getCallbackQuery().getMessage()
                            .getChatId(), "Введите данные, которые необходимы для регистрации:"));
                }
                case "SEX_BUTTON" -> {
                    return Optional.ofNullable(registerService.sendSexChoiceMessage(update.getCallbackQuery().getMessage()
                            .getChatId(), "Выберите пол"));
                }
                case "MALE_BUTTON" -> {
                    bio.put("sex", "мужской");
                    result.setText("Пол сохранен");
                }
                case "FEMALE_BUTTON" -> {
                    bio.put("sex", "женский");
                    result.setText("Пол сохранен");
                }
                case "PHONE_BUTTON" -> {
                    result.setText("Введите номер телефона, например 89173456954 ");
                }
                case "DATE_OF_BIRTH_BUTTON" -> {
                    result.setText("Введите дату рождения, например 01-01-2001");

                }
                case "CALENDAR.NEXT_MONTH", "CALENDAR.PREV_MONTH" -> {
                    return Optional.ofNullable(recordsService.sendDayChoiceMessage(update, false));
                }
                case "CUSTOMERS_BUTTON" -> {
                    String date = appointment.get("adminDateTime");
                    LocalDate currentDay = appointmentService.processDate(date);
                    LocalDateTime currentDateTime = LocalDateTime.of(currentDay, LocalTime.of(0, 0));
                    LocalDateTime nextDay = currentDateTime.plusDays(1);

                    List<SendMessage> sendMessage = appointmentService.showAppointmentsForDay(update, currentDateTime, nextDay);

                    if (sendMessage.get(0).getText().equals("Записей нет")) {
                        result.setText(sendMessage.get(0).getText());
                        return Optional.ofNullable(result);
                    }

                    for (int i = 0; i < sendMessage.size(); i++) {
                        telegramBot.sendMessage(sendMessage.get(i));
                    }
                }
                case "ADMIN_MAIN_MENU_BUTTON", "USER_MAIN_MENU_BUTTON" -> {
                    if (message.getText().equals("ADMIN_MAIN_MENU_BUTTON")) {
                        message.setReplyMarkup(InlineKeyboardMarkup.builder()
                                .keyboardRow(List.of(InlineKeyboardButton.builder()
                                        .text("Вы находитесь в панели администратора")
                                        .callbackData(ADMIN_MAIN_MENU_BUTTON)
                                        .build()))
                                .build());
                    } else {
                        message.setReplyMarkup(InlineKeyboardMarkup.builder()
                                .keyboardRow(List.of(InlineKeyboardButton.builder()
                                        .text("Вы находитесь в главном меню")
                                        .callbackData(USER_MAIN_MENU_BUTTON)
                                        .build()))
                                .build());

                    }
                }

                case "MY_PROFILE_BUTTON" -> {
                    return Optional.ofNullable(informationService.sendInformationMessage(update.getCallbackQuery().getMessage()
                            .getChatId()));
                }

                case "MAIN_MENU_BUTTON" -> {
                    return Optional.ofNullable(recordsService.userKeyBoard(result));
                }

                case "SIGN_UP_FOR_GAME_BUTTON" -> {
                    return Optional.ofNullable(recordsService.sendRecordsCalendarMessage(update, false));
                }

                case "ABOUT_THE_SITE_BUTTON" -> {
                    Long id = update.getCallbackQuery().getFrom().getId();
                    return Optional.ofNullable(SendMessage.builder()
                            .text(command.get("/about_the_site").execute(update))
                            .chatId(id)
                            .replyMarkup(InlineKeyboardMarkup.builder()
                                    .keyboardRow(List.of(InlineKeyboardButton.builder()
                                            .callbackData(MAIN_MENU_BUTTON)
                                            .text("В главное меню")
                                            .build()))
                                    .build())
                            .build());
                }

                case "TARIFFS_BUTTON" -> {
                    Long id = update.getCallbackQuery().getFrom().getId();
                    return Optional.ofNullable(SendMessage.builder()
                            .text(command.get("/tariffs").execute(update))
                            .chatId(id)
                            .replyMarkup(InlineKeyboardMarkup.builder()
                                    .keyboardRow(List.of(InlineKeyboardButton.builder()
                                            .callbackData(MAIN_MENU_BUTTON)
                                            .text("В главное меню")
                                            .build()))
                                    .build())
                            .build());
                }


                default -> {
                    result.setText("Вы ничего не выбрали");
                }
            }

        }
        checkBio();
        return Optional.of(result);
    }

    private void checkBio() {
        if (bio.containsKey("username") &&
                bio.containsKey("name") &&
                bio.containsKey("surname") &&
                bio.containsKey("phone") &&
                bio.containsKey("dateOfBirth")) {
            long chatId = Long.parseLong(bio.get("chatId"));
            if (userRepository.findById(Long.parseLong(bio.get("chatId"))).isEmpty()) {
                try {
                    User user = new User();
                    user.setId(Long.valueOf(bio.get("chatId")));
                    user.setUsername(bio.get("username"));
                    user.setName(bio.get("name"));
                    user.setSurname(bio.get("surname"));
                    user.setPhone(bio.get("phone"));

                    if (bio.get("sex").equalsIgnoreCase("мужской")) {
                        user.setSex(User.Sex.MALE);
                    } else {
                        user.setSex(User.Sex.FEMALE);
                    }

                    String inputDateOfBirth = bio.get("dateOfBirth");
                    SimpleDateFormat format = null;
                    if (inputDateOfBirth.matches("^\\d{2}-\\d{2}-\\d{4}$")) {
                        format = new SimpleDateFormat("dd-MM-yyyy");
                    } else if (inputDateOfBirth.matches("^\\d{2}\\.\\d{2}\\.\\d{4}$")) {
                        format = new SimpleDateFormat("dd.MM.yyyy");
                    } else if (inputDateOfBirth.matches("^\\d{2}\\.\\d{2}\\.\\d{2}$")) {
                        format = new SimpleDateFormat("dd.MM.yy");
                    } else if (inputDateOfBirth.matches("^\\d{2}-\\d{2}-\\d{2}$")) {
                        format = new SimpleDateFormat("dd-MM-yy");
                    }
                    Date resultDateOfBirth = format.parse(bio.get("dateOfBirth"));
                    user.setDateOfBirth(resultDateOfBirth);


                    user.setStatus(User.Status.UNCONFIRMED);
                    userRepository.save(user);
                    log.info("{} saved, bio: {}", user, bio);
                    bio.clear();
                    SendMessage result = new SendMessage();
                    result.setChatId(chatId);
                    result.setText("Регистрация окончена");
                    recordsService.userKeyBoard(result);
                    telegramBot.sendMessage(result);
                } catch (ParseException e) {
                    log.error("Error during date parsing");
                }
            }
        }
    }

    public void checkUserFields(SendMessage result) {
        InlineKeyboardMarkup markupInLine = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowsInLine = new ArrayList<>();

        if (!bio.containsKey("phone")) {
            rowsInLine.add(List.of(rowsAndButtons.addButtonToRow("Телефон", RowsAndButtonsService.PHONE_BUTTON)));
        }
        if (!bio.containsKey("sex")) {
            rowsInLine.add(List.of(rowsAndButtons.addButtonToRow("Пол", RowsAndButtonsService.SEX_BUTTON)));
        }
        if (!bio.containsKey("dateOfBirth")) {
            rowsInLine.add(List.of(rowsAndButtons.addButtonToRow("Дата рождения", RowsAndButtonsService.DATE_OF_BIRTH_BUTTON)));
        }
        markupInLine.setKeyboard(rowsInLine);
        result.setReplyMarkup(markupInLine);

        if (bio.containsKey("phone") &&
                bio.containsKey("sex") &&
                bio.containsKey("dateOfBirth")) {
            checkBio();
        }
    }

    public void registerBot(TelegramBot telegramBot) {
        this.telegramBot = telegramBot;
    }

    public SendMessage sendMessage(long chatId, String response) {
        SendMessage sendMessage = rowsAndButtons.createSendMessage(chatId, response);

        return sendMessage;
    }
}
