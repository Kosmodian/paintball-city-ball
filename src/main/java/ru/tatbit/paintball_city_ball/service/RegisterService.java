package ru.tatbit.paintball_city_ball.service;

import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.util.List;

import static ru.tatbit.paintball_city_ball.service.RowsAndButtonsService.*;

@Service
public class RegisterService {

    private final RowsAndButtonsService rowsAndButtons;

    public RegisterService(RowsAndButtonsService rowsAndButtons) {
        this.rowsAndButtons = rowsAndButtons;
    }

    public SendMessage sendInviteForRegisterMessage(long chatId, String responce) {
        SendMessage sendMessage = rowsAndButtons.createSendMessage(chatId, responce);

        InlineKeyboardMarkup markupInLine = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowsInLine = rowsAndButtons.addRows(1);

        rowsInLine.get(0).add(rowsAndButtons.addButtonToRow("Зарегистрироваться", REGISTER_BUTTON));

        markupInLine.setKeyboard(rowsInLine);
        sendMessage.setReplyMarkup(markupInLine);

        return sendMessage;

    }

    public SendMessage sendRegisterMessage(long chatId, String responce) {
        SendMessage sendMessage = rowsAndButtons.createSendMessage(chatId, responce);

        InlineKeyboardMarkup markupInLine = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowsInLine = rowsAndButtons.addRows(3);

        rowsInLine.get(0).add(rowsAndButtons.addButtonToRow("Пол", SEX_BUTTON));
        rowsInLine.get(1).add(rowsAndButtons.addButtonToRow("Телефон", PHONE_BUTTON));
        rowsInLine.get(2).add(rowsAndButtons.addButtonToRow("Дата рождения", DATE_OF_BIRTH_BUTTON));

        markupInLine.setKeyboard(rowsInLine);
        sendMessage.setReplyMarkup(markupInLine);

        return sendMessage;

    }

    public SendMessage sendSexChoiceMessage(long chatId, String responce) {
        SendMessage sendMessage = rowsAndButtons.createSendMessage(chatId, responce);

        InlineKeyboardMarkup markupInLine = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowsInLine = rowsAndButtons.addRows(1);

        rowsInLine.get(0).add(rowsAndButtons.addButtonToRow("Мужской", MALE_BUTTON));
        rowsInLine.get(0).add(rowsAndButtons.addButtonToRow("Женский", FEMALE_BUTTON));

        markupInLine.setKeyboard(rowsInLine);
        sendMessage.setReplyMarkup(markupInLine);

        return sendMessage;

    }

    public EditMessageText checkCallbackQueryFromUser(Update update) {
        String callbackData = update.getCallbackQuery().getData();
        long messageId = update.getCallbackQuery().getMessage().getMessageId();
        long chatIdFromQuery = update.getCallbackQuery().getMessage().getChatId();
        EditMessageText message = new EditMessageText();
        String text = callbackData.isEmpty() ? "Command unknown " : callbackData;

        message.setChatId(chatIdFromQuery);
        message.setText(text);
        message.setMessageId((int) messageId);

        return message;
    }

}
