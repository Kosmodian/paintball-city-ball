package ru.tatbit.paintball_city_ball.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.tatbit.paintball_city_ball.entity.User;

import java.util.Optional;
import java.util.Set;

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsername(String username);

    Optional<User> findById(Long id);

    @Query(value = "select tur.roles from tb_users_roles tur left join tb_users tu on tur.user_id = tu.id where tu.id = ?", nativeQuery = true)
    Set<User.Role> checkRolesByUserId(long id);

    @Query(value = "select tur.roles from tb_users tu join tb_users_roles tur on tu.id = tur.user_id where tu.username = ?", nativeQuery = true)
    Set<User.Role> checkRolesByUsername(String username);
}
