package ru.tatbit.paintball_city_ball.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.tatbit.paintball_city_ball.entity.Appointment;

import java.time.LocalDateTime;
import java.util.List;

public interface AppointmentRepository extends JpaRepository<Appointment, Long> {
    List<Appointment> findByVisitDateTimeBetween(LocalDateTime from, LocalDateTime to);

    List<Appointment> findByUser_Id(Long id);

    Appointment findByVisitDateTime(LocalDateTime dateTime);
}
