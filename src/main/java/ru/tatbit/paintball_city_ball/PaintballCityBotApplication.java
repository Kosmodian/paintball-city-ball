package ru.tatbit.paintball_city_ball;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PaintballCityBotApplication {

    public static void main(String[] args) {
        SpringApplication.run(PaintballCityBotApplication.class, args);
    }

}
